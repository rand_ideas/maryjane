import time
import datetime

import random
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.dates as mdates
import matplotlib.animation as animation
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import tkinter as Tk
import json
import math
import random



from classes.gui import GUI

v=2.0

SERIAL_TEST = True
last_values = {"H":0, "T":0}
MAX_SIZE = -6000

if SERIAL_TEST:
    import serial
    arduino = \
         serial.Serial("/dev/ttyACM0", \
         baudrate=9600, timeout=1.5, write_timeout=1.5)

    time.sleep(2)


back_time = datetime.timedelta(hours=4)
fut_time = datetime.timedelta(minutes=30)
background_color = 'black'
foreground_color = 'white'
foreground_green = 'green'



y_Data = {"g0": [], "g1": []}
x_Data = []


GRAPH_PARAMS = \
    {"0": {"Temperature":
            {"max":50, "min": 0, "major": 5, "minor": 1 }}, \
     "1": {"Humidity":
            {"max":100,  "min": 0, "major": 10, "minor": 5 }}}

CONTROLS = {
            "TEMPERATURE":
                {"default_on": 27, "default_off": 24,
                 "max_on":28, "min_on": 25,
                 "max_off":25, "min_off": 20
                 },
            "HUMIDITY":
                {"default_on": 60, "default_off": 75,
                "max_off":75, "min_off": 50,
                "max_on":49, "min_on": 25,},
            "LIGHT":
                {"default_on": 8, "default_off": 20},
            "PUMP":
                {"default_on": 2, "default_off": 30}
}





#-------------------------------GUI----------------------------------
root = Tk.Tk()
root.title("MaryJane Controller")
width, height = root.winfo_screenwidth(), root.winfo_screenheight()

root.geometry('%dx%d+0+0' % (width,height))

    #-------------------GRAPHS---------------------------

if width > 480:
    f = 3.0

else:
    f = 1

fig = plt.Figure(facecolor=background_color, figsize=(3*f, 2.5*f))
ax = []
ax.append(fig.add_subplot(211))
ax.append(fig.add_subplot(212))


line0, = ax[0].plot([], [], lw=2, color='g')
line1, = ax[1].plot([], [], lw=2, color='g')
lines = [line0, line1]

canvas = FigureCanvasTkAgg(fig, master=root)
canvas.draw()
canvas.get_tk_widget().grid(column=2,columnspan=4, rowspan=12, \
                            row=0, sticky="ne")

    #----------------------------------------------------

gui = GUI(root, background_color, foreground_color, \
          foreground_green, CONTROLS, width, height)

#--------------------------------------------------------------------




#Loop to initilize plots parameters
i = 0;
#fig.autofmt_xdate()

for a in ax:

    a.grid(color='white', linestyle='-', linewidth=0.25)
    a.set_facecolor('xkcd:'+ background_color)

    a.set_title(list(GRAPH_PARAMS[str(i)].keys())[0], \
                color=foreground_color,size=6)

    h_fmt = mdates.DateFormatter('%H:%M')
    a.xaxis.set_major_formatter(h_fmt)


    hours = mdates.HourLocator(interval = 1)
    a.xaxis.set_major_locator(hours)

    minutes = mdates.MinuteLocator(byminute=[10,20,30,40,50])
    a.xaxis.set_minor_locator(minutes)
    #a.xaxis.set_major_locator(minutes)

    formatter = plt.FuncFormatter( \
                lambda x, loc: "{:,.0f}".format(int(x)))
    a.yaxis.set_major_formatter(formatter)
    a.yaxis.set_major_locator(ticker.MultipleLocator\
            (GRAPH_PARAMS[str(i)][a.get_title()]["major"]))
    a.yaxis.set_minor_locator(ticker.MultipleLocator\
            (GRAPH_PARAMS[str(i)][a.get_title()]["minor"]))

    a.set_ylim(GRAPH_PARAMS[str(i)][a.get_title()]["min"], \
                GRAPH_PARAMS[str(i)][a.get_title()]["max"])

    a.tick_params(axis='x', colors=foreground_color, labelsize=6)
    a.tick_params(axis='y', colors=foreground_color, labelsize=6)

    a.spines['bottom'].set_color(foreground_color)
    a.spines['top'].set_color(foreground_color)
    a.spines['left'].set_color(foreground_color)
    a.spines['right'].set_color(foreground_color)


    i += 1
fig.tight_layout()




def read_port():

    try:
        val_read_data = arduino.readline().decode("utf-8")
        val_read_data = val_read_data.replace("'",'"')
        #print(val_read_data)
        j = json.loads(val_read_data)
        last_values.update(j)

        return j["T"], j["H"]

    except UnicodeDecodeError:
        print('uh oh - UNICODE')

    except ValueError:
        #print('uh oh - JSON')
        pass

    return last_values["T"], last_values["H"]

def send_control():


    data = gui.get_all_values()

    arduino.write(data. \
        encode('utf-8'))


def data_gen():

    x = datetime.datetime.now()

    if not SERIAL_TEST:
        t = random.randrange(50)
        h = random.randrange(50)
        print(gui.get_all_values())


    else:
        t, h = read_port();
        send_control()


    yield x, t, h


def animate(data):
    global x_Data
    global y_Data

    x, t, h = data
    x_Data.append(x)
    x_Data = x_Data[MAX_SIZE:]

    gui.update_temp_hum(t, h)
    y = [t, h]

    for l in range(len(lines)):
        ax[l].set_xlim(x - back_time, x + fut_time)
        y_Data["g"+str(l)].append(y[l])
        y_Data["g"+str(l)] = y_Data["g"+str(l)][MAX_SIZE:]
        lines[l].set_data(x_Data, y_Data["g"+str(l)])

    return lines


ani = animation.FuncAnimation(fig, animate, data_gen, interval=5000, blit=False)

Tk.mainloop()
