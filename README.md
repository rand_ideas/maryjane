# maryjane
Small control program for better gardening your "special" plant (Weed)

It controls temperature, humidity and watering.

It is still in development.




## For installing python3 and Tkinter
Helpful info here: https://askubuntu.com/questions/1224230/how-to-install-tkinter-for-python-3-8


```
sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev python-tk python3-tk tk-dev
```
```
wget https://www.python.org/ftp/python/3.8.0/Python-3.8.0.tgz
sudo tar zxf Python-3.8.0.tgz
cd Python-3.8.0
```

```
sudo ./configure --with-tcltk-includes='-I/usr/include -I/usr/include/tcl' --with-tcltk-libs='-L/usr/lib -ltcl -ltk' --enable-optimizations
```

```
sudo make -j 4
sudo make install
sudo nano ~/.bashrc
  alias python3=python3.8
source ~/.bashrc
```


## Pin configurations

- #define temperature_out 4
- #define humidity_out 5
- #define light_out 6
- #define Pump 7


## For using DHT sensor (TEMP /HUM)
Download arduino library  DHTLib.zip




https://stackoverflow.com/questions/47200625/how-to-make-ttk-scale-behave-more-like-tk-scale

## For enabling raspberry 3.5inc screen
```
git clone https://github.com/goodtft/LCD-show.git
sudo chmod -R 755 LCD-show
cd LCD-show/
sudo ./LCD35-show
```


## Dependencies needed to run program
```
sudo pip3 install numpy
sudo pip3 install matplot
sudo pip3 install pyserial
```
## Find address name for serial port
```
ls /dev/tty*
```

## Change time settings in raspberry
sudo raspi-config

## Adjust menu bar to maximaze screen
Rightclick menu bar /advance/minimze panel when not in use





## Service script made for updating program on boot
Run sh script to pull new updates from git.

If failed, try again after 10s indefinitely

Then start program

Some info gotten from:
https://www.digikey.com/en/maker/projects/how-to-run-a-raspberry-pi-program-on-startup/cc16cb41a3d447b8aaacf1da14368b13



Create file
```
sudo nano /lib/systemd/system/maryjane.service
```



```
[Unit]
Description=MaryJane Service

[Service]
Type=simple
Environment=DISPLAY=:0
Environment=XAUTHORITY=/home/pi/.Xauthority
ExecStart=/usr/local/bin/python3 /home/pi/maryjane/mary.py > /home/pi/maryjane.log 2>&1
ExecStop=/home/pi/maryjane/pull.sh > /home/pi/maryjane.log 2>&1
Restart=always
RestartSec=10s
KillMode=process
TimeoutSec=infinity
User=pi

[Install]
WantedBy=graphical.target
```
Change permissions in service file

Reload and enable Service

Reboot to test

```
sudo chmod 644 /lib/systemd/system/maryjane.service
sudo systemctl daemon-reload
sudo systemctl enable maryjane.service
sudo reboot
```

For debugging:
- Start service without rebooting
- Check logs from service
- Check logs from service
```
systemctl start maryjane.service
sudo journalctl _SYSTEMD_UNIT=maryjane.service
systemctl status maryjane.service
```



## Bash script to pull updates on boot

```
#!/bin/sh
set -e
if ping -c 3 -W 10 8.8.8.8 1>/dev/null; then
  cd /home/pi/maryjane/
  /usr/bin/git pull
  exit 0
else
    echo "Ping not succesfull" >&2
    exit 1
fi
```
Make it executable:
```
chmod +x /home/pi/maryjane/pull.sh
```
