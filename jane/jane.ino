
#include <dht.h>


//Max analog pins to read
#define MAX_PINS 0

#define DHT11_PIN A0     // what pin we're connected to
dht DHT;

//Variables
//Temp & Hum variables

int temp_in = 0;
int hum_in = 0;
int chk;



// Define relay pins:
#define temperature_out 4
#define humidity_out 5
#define light_out 6
#define pump_out 7



//Variables to receive data from serial
char d_or_s = '-';     //variable to know which array is filling up

const byte numChars = 128;//16CHARS X 8

char receivedChars_ST[numChars];   // an array to store the received data
char receivedChars_St[numChars];   // an array to store the received data
char receivedChars_SH[numChars];   // an array to store the received data
char receivedChars_Sh[numChars];   // an array to store the received data
char receivedChars_SL[numChars];   // an array to store the received data
char receivedChars_Sl[numChars];   // an array to store the received data
char receivedChars_SP[numChars];   // an array to store the received data
char receivedChars_Sp[numChars];   // an array to store the received data

float sT = 0.0;
float st = 0.0;
float sH = 0.0;
float sh = 0.0;
float sL = 0.0;
float sl = 0.0;
float sP = 0.0;
float sp = 0.0;

String r_ST = "";
String r_St = "";
String r_SH = "";
String r_Sh = "";
String r_SL = "";
String r_Sl = "";
String r_SP = "";
String r_Sp = "";


static byte ndx_ST = 0; //Index por positioning array received
static byte ndx_St = 0; //Index por positioning array received
static byte ndx_SH = 0; //Index por positioning array received
static byte ndx_Sh = 0; //Index por positioning array received
static byte ndx_SL = 0; //Index por positioning array received
static byte ndx_Sl = 0; //Index por positioning array received
static byte ndx_SP = 0; //Index por positioning array received
static byte ndx_Sp = 0; //Index por positioning array received



//For reading analog inputs
int n[MAX_PINS];

//index = is for TEMP and HUM
int sensor_index = 1;
int a[MAX_PINS];

int oversamples = 16;

unsigned int sum[MAX_PINS];

float send_data_clock = millis();
float send_data_interval = 5000.0;



void read_all_pins();
void read_serial();
void traduce_serial();
void send_data_to_serial();
void control();
void read_temp_hum();


void setup()
   {
     Serial.begin(9600);

     pinMode(temperature_out, OUTPUT);
     pinMode(humidity_out, OUTPUT);
     pinMode(light_out, OUTPUT);
     pinMode(pump_out, OUTPUT);



   }

void loop()
   {

     //read_all_pins();

     if(send_data_clock + send_data_interval < millis()){
        chk = DHT.read11(DHT11_PIN);

        //Serial.print("Humidity (%): ");
        //Serial.println((float)DHT.humidity, 2);

        //Serial.print("Temperature (C): ");
        //Serial.println((float)DHT.temperature, 2);

        read_temp_hum();
        send_data_to_serial();
        send_data_clock = millis();

     }


     read_serial();
     traduce_serial();
     control();



   }

void read_temp_hum(){


  //Serial.print("Temperature = ");
  //Serial.println(DHT.temperature);
  //Serial.print("Humidity = ");
  //Serial.println(DHT.humidity);

  temp_in = DHT.temperature;
  hum_in = DHT.humidity;


}



 void read_serial(){

     char endMarker = '\n';
     char rc;

     while(Serial.available()) {

       rc = Serial.read();

       if (rc != endMarker) {

            if(rc == 'T' || (d_or_s == 'T' && rc != 't' &&
               rc != 'H' && rc != 'h' &&
               rc != 'P' && rc != 'p' &&
               rc != 'L' && rc != 'l')){
              d_or_s = 'T';
              receivedChars_ST[ndx_ST] = rc;

              ndx_ST++;
              if (ndx_ST >= numChars) {
                    ndx_ST = numChars - 1;
              }
            }

            else if(rc == 't' || (d_or_s == 't' && rc != 'T' &&
               rc != 'H' && rc != 'h' &&
               rc != 'P' && rc != 'p' &&
               rc != 'L' && rc != 'l')){
              d_or_s = 't';
              receivedChars_St[ndx_St] = rc;

              ndx_St++;
              if (ndx_St >= numChars) {
                    ndx_St = numChars - 1;
              }
            }

            else if(rc == 'H' || (d_or_s == 'H' && rc != 'h' &&
               rc != 'T' && rc != 't' &&
               rc != 'P' && rc != 'p' &&
               rc != 'L' && rc != 'l')){
              d_or_s = 'H';
              receivedChars_SH[ndx_SH] = rc;

              ndx_SH++;
              if (ndx_SH >= numChars) {
                    ndx_SH = numChars - 1;
              }
            }

            else if(rc == 'h' || (d_or_s == 'h' &&  rc != 'H' &&
               rc != 'T' && rc != 't' &&
               rc != 'P' && rc != 'p' &&
               rc != 'L' && rc != 'l')){
              d_or_s = 'h';
              receivedChars_Sh[ndx_Sh] = rc;

              ndx_Sh++;
              if (ndx_Sh >= numChars) {
                    ndx_Sh = numChars - 1;
              }
            }

            else if(rc == 'L' || (d_or_s == 'L' && rc != 'l' &&
               rc != 'H' && rc != 'h' &&
               rc != 'P' && rc != 'p' &&
               rc != 'T' && rc != 't')){
              d_or_s = 'L';
              receivedChars_SL[ndx_SL] = rc;

              ndx_SL++;
              if (ndx_SL >= numChars) {
                    ndx_SL = numChars - 1;
              }
            }

            else if(rc == 'l' || (d_or_s == 'l' && rc != 'L' &&
               rc != 'H' && rc != 'h' &&
               rc != 'P' && rc != 'p' &&
               rc != 'T' && rc != 't')){
              d_or_s = 'l';
              receivedChars_Sl[ndx_Sl] = rc;

              ndx_Sl++;
              if (ndx_Sl >= numChars) {
                    ndx_Sl = numChars - 1;
              }
            }//PUMP
            else if(rc == 'P' || (d_or_s == 'P' && rc != 'p' &&
               rc != 'H' && rc != 'h' &&
               rc != 'L' && rc != 'l' &&
               rc != 'T' && rc != 't')){
              d_or_s = 'P';
              receivedChars_SP[ndx_SP] = rc;

              ndx_SP++;
              if (ndx_SP >= numChars) {
                    ndx_SP = numChars - 1;
              }
            }
            else if(rc == 'p' || (d_or_s == 'p' && rc != 'P' &&
               rc != 'H' && rc != 'h' &&
               rc != 'L' && rc != 'l' &&
               rc != 'T' && rc != 't')){
              d_or_s = 'p';
              receivedChars_Sp[ndx_Sp] = rc;

              ndx_Sp++;
              if (ndx_Sp >= numChars) {
                    ndx_Sp = numChars - 1;
              }
            }

       }
        else {

            receivedChars_ST[ndx_ST] = '\0'; // terminate the string
            receivedChars_St[ndx_St] = '\0'; // terminate the string
            receivedChars_SH[ndx_SH] = '\0'; // terminate the string
            receivedChars_Sh[ndx_Sh] = '\0'; // terminate the string
            receivedChars_SL[ndx_SL] = '\0'; // terminate the string
            receivedChars_Sl[ndx_Sl] = '\0'; // terminate the string
            receivedChars_SP[ndx_SP] = '\0'; // terminate the string
            receivedChars_Sp[ndx_Sp] = '\0'; // terminate the string

            ndx_ST = 0;
            ndx_St = 0;
            ndx_SH = 0;
            ndx_Sh = 0;
            ndx_SL = 0;
            ndx_Sl = 0;
            ndx_SP = 0;
            ndx_Sp = 0;


            r_ST = String(receivedChars_ST);
            r_St = String(receivedChars_St);
            r_SH = String(receivedChars_SH);
            r_Sh = String(receivedChars_Sh);
            r_SL = String(receivedChars_SL);
            r_Sl = String(receivedChars_Sl);
            r_SP = String(receivedChars_SP);
            r_Sp = String(receivedChars_Sp);


        }
     }

 }

 void traduce_serial(){


      if(r_ST.length() > 0 && r_St.length() > 0 &&
         r_SH.length() > 0 && r_Sh.length() > 0 &&
         r_SL.length() > 0 && r_Sl.length() > 0 ){


           if(r_ST.substring(0,1) == "T"){
              sT = (r_ST.substring(1)).toInt();

            }
           if(r_St.substring(0,1) == "t"){
              st = (r_St.substring(1)).toInt();

            }
           if(r_SH.substring(0,1) == "H"){
              sH = (r_SH.substring(1)).toInt();

            }

            if(r_Sh.substring(0,1) == "h"){
               sh = (r_Sh.substring(1)).toInt();

             }
            if(r_SL.substring(0,1) == "L"){
               sL = (r_SL.substring(1)).toInt();

             }
            if(r_Sl.substring(0,1) == "l"){
               sl = (r_Sl.substring(1)).toInt();

             }
             if(r_SP.substring(0,1) == "P"){
                sP = (r_SP.substring(1)).toInt();

              }
             if(r_Sp.substring(0,1) == "p"){
                sp = (r_Sp.substring(1)).toInt();

              }

            //T99t88H77h66L55l44
            //T25t24H22h21L1l0P1p0  //turns on all
            //T30t29H26h25L0l1P0p1 //Turns off all
            //Serial.println(sT);
            //Serial.println(st);
            //Serial.println(sH);
            //Serial.println(sh);
            //Serial.println(sL);
            //Serial.println(sl);
            //Serial.println(sP);
            //Serial.println(sp);

            r_ST = String("");
            r_St = String("");
            r_SH = String("");
            r_Sh = String("");
            r_SL = String("");
            r_Sl = String("");
            r_SP = String("");
            r_Sp = String("");

      }

   }

void send_data_to_serial(){

      Serial.print("{");

      for (int i=0; i<MAX_PINS; i++){

        Serial.print("'a");
        Serial.print(i);
        Serial.print("':");
        Serial.print((a[i]));

        if(i < MAX_PINS){ // i+1 if don want to send dt *
          Serial.print(",");

        }
      }

      Serial.print("'T'");
      Serial.print(":");
      Serial.print(temp_in);
      Serial.print(",");
      Serial.print("'H'");
      Serial.print(":");
      Serial.print(hum_in);

      Serial.println("}");
}


void read_all_pins(){

      int a_oversampled = 0;

      for(int i=0; i<oversamples; i++){

        a_oversampled += analogRead(sensor_index);

      }

      sum[sensor_index] += a_oversampled;
      n[sensor_index] += 1;

      if(sensor_index >= MAX_PINS){
        sensor_index = 0;

      }
      else{
        sensor_index += 1;
      }

      //index = is for TEMP and HUM
      for(int i=1; i<MAX_PINS; i++){

         float accom = 0.0;

         if(a[i] > 0){
            accom = a[i];
         }

         a[i] = (sum[i]/n[i] + accom)/2;


      }

      //index = is for TEMP and HUM
      for(int i=1; i<MAX_PINS; i++){

          n[i] = 0;
          sum[i] = 0;

       }

 }

void control(){

  int TControl = (int)sT;
  int tControl = (int)st;
  int HControl = (int)sH;
  int hControl = (int)sh;
  int LControl = (int)sL;
  int lControl = (int)sl;
  int PControl = (int)sP;
  int pControl = (int)sp;


  if(TControl > 0 && TControl > tControl && TControl <= temp_in){
    
    digitalWrite(temperature_out, HIGH);

  }
  else if(tControl > 0 && TControl > tControl && tControl >= temp_in){

    digitalWrite(temperature_out, LOW);

  }

  if(HControl > 0 && HControl > hControl && HControl <= hum_in){
    
    digitalWrite(humidity_out, HIGH);

  }
  else if(hControl > 0 && HControl > hControl && hControl >= hum_in){

    digitalWrite(humidity_out, LOW);

  }

  if(LControl > 0 && LControl != lControl){
    
    digitalWrite(light_out, HIGH);

  }
  else if(lControl > 0 && LControl != lControl){

    digitalWrite(light_out, LOW);

  }

  if(PControl > 0 && PControl != pControl){
    
    digitalWrite(pump_out, HIGH);

  }
  else if(pControl > 0 && PControl != pControl){

    digitalWrite(pump_out, LOW);

  }


}
