#!/bin/sh
set -e
if ping -c 3 -W 10 8.8.8.8 1>/dev/null; then
  cd /home/pi/maryjane/
  /usr/bin/git pull
  exit 0
else
    echo "Ping not succesfull" >&2
    exit 1
fi
