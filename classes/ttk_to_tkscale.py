import tkinter as tk
import tkinter.ttk as ttk

class TtkScale(ttk.Frame):
    def __init__(self, master=None, **kwargs):

        ttk.Frame.__init__(self, master)


        self.columnconfigure(0, weight=1)
        self.showvalue = kwargs.pop('showvalue', True)
        self.tickinterval = kwargs.pop('tickinterval', 0)
        self.digits = kwargs.pop('digits', '0')
        self.interval10 = kwargs.pop('interval10', False)



        self.label_forscale = kwargs.pop('label_forscale', "")

        one_more_row = 0
        if self.label_forscale != "":
            one_more_row = 1


        # create frame style
        s = ttk.Style()
        # Create style used by default for all Frames
        s.configure(master, background=kwargs.pop('bgL', "green"))
        s.configure("labelValue.TLabel",
                foreground=kwargs.pop('fgL', "green"),
                font=kwargs.pop('fontL', "Courier 10"))


        if 'command' in kwargs:
            # add self.display_value to the command
            fct = kwargs['command']

            def cmd(value):
                fct(value)
                self.display_value(value)

            kwargs['command'] = cmd
        else:
            kwargs['command'] = self.display_value

        self.scale = ttk.Scale(self, **kwargs)

        ttk.Label(self, text=self.label_forscale, style="labelValue.TLabel").grid(row=0, sticky="swe")

        # get slider length
        style = ttk.Style(self)
        style_name = kwargs.get('style', '%s.TScale' % (str(self.scale.cget('orient')).capitalize()))
        self.sliderlength = style.lookup(style_name, 'sliderlength', default=30)

        self.extent = kwargs['to'] - kwargs['from_']
        self.start = kwargs['from_']
        # showvalue
        if self.showvalue:
            ttk.Label(self, text=' ').grid(row=0+one_more_row, sticky="nwe")
            self.label = ttk.Label(self, text='0',style="labelValue.TLabel")

            self.label.place(in_=self.scale, bordermode='outside', x=0, y=0, anchor='s')

            self.display_value(self.scale.get())

        self.scale.grid(row=1+one_more_row, sticky='ew')

        # ticks
        if self.tickinterval:
            ttk.Label(self, text=' ').grid(row=2+one_more_row)
            self.ticks = []
            self.ticklabels = []
            nb_interv = round(self.extent/self.tickinterval)
            formatter = '{:.' + str(self.digits) + 'f}'
            for i in range(nb_interv + 1):
                tick = kwargs['from_'] + i * self.tickinterval
                self.ticks.append(tick)
                self.ticklabels.append(ttk.Label(self, text=formatter.format(tick)))
                self.ticklabels[i].place(in_=self.scale, bordermode='outside', x=0, rely=1, anchor='n')
            self.place_ticks()

        self.scale.bind('<Configure>', self.on_configure)
        self.scale.bind('<ButtonRelease-1>', self.click_release)
        self.scale.bind('<Leave>', self.click_release)


    def click_release(self, event):
        self.display_value((self.scale.get()))


    def convert_to_pixels(self, value):

        return ((value - self.start)/ self.extent) * (self.scale.winfo_width()- self.sliderlength) + self.sliderlength / 2

    def set(self, value):

        self.display_value((self.scale.get()))
        return self.scale.set((value))

    def get(self):
        self.display_value((self.scale.get()))
        return int(round(self.scale.get(),0))

    def display_value(self, value):

        # position (in pixel) of the center of the slider
        x = self.convert_to_pixels(float(value))

        if self.interval10:
            value = int(float(value)/10)*10

        # pay attention to the borders
        half_width = self.label.winfo_width() / 2
        if x + half_width > self.scale.winfo_width():
            x = self.scale.winfo_width() - half_width
        elif x - half_width < 0:
            x = half_width
        self.label.place_configure(x=x)
        formatter = '{:.' + str(self.digits) + 'f}'
        self.label.configure(text=formatter.format(float(value)))

    def place_ticks(self):
        # first tick
        tick = self.ticks[0]
        label = self.ticklabels[0]
        x = self.convert_to_pixels(tick)
        half_width = label.winfo_width() / 2
        if x - half_width < 0:
            x = half_width
        label.place_configure(x=x)
        # ticks in the middle
        for tick, label in zip(self.ticks[1:-1], self.ticklabels[1:-1]):
            x = self.convert_to_pixels(tick)
            label.place_configure(x=x)
        # last tick
        tick = self.ticks[-1]
        label = self.ticklabels[-1]
        x = self.convert_to_pixels(tick)
        half_width = label.winfo_width() / 2
        if x + half_width > self.scale.winfo_width():
            x = self.scale.winfo_width() - half_width
        label.place_configure(x=x)

    def on_configure(self, event):
        """Redisplay the ticks and the label so that they adapt to the new size of the scale."""
        self.display_value(int(self.scale.get()))
        #self.place_ticks()
