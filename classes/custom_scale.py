import tkinter as tk
import tkinter.ttk as ttk

class TtkScale(ttk.Frame):
    def __init__(self, master=None, **kwargs):

        ttk.Frame.__init__(self, master)


        self.columnconfigure(0, weight=1)
        self.showvalue = kwargs.pop('showvalue', True)
        self.tickinterval = kwargs.pop('tickinterval', 0)
        self.digits = kwargs.pop('digits', '0')



        # create frame style
        s = ttk.Style()
        # Create style used by default for all Frames
        s.configure(master, background=kwargs.pop('bgL', "green"))
        s.configure("labelValue.TLabel",
                foreground=kwargs.pop('fgL', "green"),
                font=kwargs.pop('fontL', "Courier 10"))





        if 'command' in kwargs:
            # add self.display_value to the command
            fct = kwargs['command']

            def cmd(value):
                fct(value)
                self.display_value(value)

            kwargs['command'] = cmd
        else:
            kwargs['command'] = self.display_value

        self.scale = ttk.Scale(self, **kwargs)

        # get slider length
        style = ttk.Style(self)
        style_name = kwargs.get('style', '%s.TScale' % (str(self.scale.cget('orient')).capitalize()))
        self.sliderlength = style.lookup(style_name, 'sliderlength', default=30)

        self.extent = kwargs['to'] - kwargs['from_']
        self.start = kwargs['from_']
        # showvalue
        if self.showvalue:
            ttk.Label(self, text=' ').grid(row=0, sticky="nwe")
            self.label = ttk.Label(self, text='0',style="labelValue.TLabel")

            self.label.place(in_=self.scale, bordermode='outside', x=0, y=0, anchor='s')
            self.display_value(self.scale.get())

        self.scale.grid(row=1, sticky='ew')



        self.scale.bind('<Configure>', self.on_configure)



    def convert_to_pixels(self, value):
        return ((value - self.start)/ self.extent) * (self.scale.winfo_width()- self.sliderlength) + self.sliderlength / 2

    def set(self, value):
        return self.scale.set(value)

    def get(self):
        return int(self.scale.get())

    def display_value(self, value):
        # position (in pixel) of the center of the slider
        x = self.convert_to_pixels(float(value))
        # pay attention to the borders
        half_width = self.label.winfo_width() / 2
        if x + half_width > self.scale.winfo_width():
            x = self.scale.winfo_width() - half_width
        elif x - half_width < 0:
            x = half_width
        self.label.place_configure(x=x)
        formatter = '{:.' + str(self.digits) + 'f}'
        self.label.configure(text=formatter.format(float(value)))



    def on_configure(self, event):
        """Redisplay the ticks and the label so that they adapt to the new size of the scale."""
        self.display_value(self.scale.get())
        #self.place_ticks()
