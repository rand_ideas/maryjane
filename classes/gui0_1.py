import tkinter as Tk
from tkinter import messagebox as mb
from tkinter import ttk
from ttkwidgets import TickScale
import datetime
import time
from threading import Timer
from classes.ttk_to_tkscale import TtkScale

class GUI():
    def __init__(self, root, background_color, foreground_color, \
                    foreground_blue, CONTROLS, width, height):



        # Base size
        normal_width = 1280  #480
        normal_height = 800  #320
        # Get percentage of screen size from Base size
        percentage_width = width / (normal_width / 100)
        percentage_height = height / (normal_height / 100)
        # Make a scaling factor, this is bases on average percentage from
        # width and height.
        scale_factor = ((percentage_width + percentage_height) / 2) / 100


        #4columns
        MIN_COLS = 6
        min_col_size = int(normal_width/MIN_COLS*scale_factor)
        min_row_size = int(100*scale_factor)
        text_offset = min_row_size/2 + 5

        font_bold_big = "Courier " + str(int(50*scale_factor)) + " bold"
        font_bold = "Courier " + str(int(20*scale_factor)) + " bold"
        font = "Courier " + str(int(20*scale_factor))

        self.DEGREE_S = "˚"

        distance_in_between = min_col_size/4



        #--------------------------FORMAT SCALES -----------------------------
        #('aqua', 'clam', 'alt', 'default', 'classic')
        style = ttk.Style(root)
        style.theme_use('alt')


        style.configure('my.Horizontal.TScale', \
            #troughcolor=foreground_color,
            background='black',
            foreground='white',
            highlightthickness='20',
            #font=('Helvetica', 18, 'bold'),
            #highlightbackground = "green",\
            #font=font_bold, \
            #borderwidth=int(scale_factor*10))
            sliderthickness=int(scale_factor*40))

        style.map("my.Horizontal.TScale",

            background=[('disabled', 'magenta'),
                    ('pressed', '!focus', 'cyan'),
                    ('active', 'green')])


        #-----------------------------------------------------------------



        root.configure(bg=background_color)
        root.columnconfigure([x for x in range(MIN_COLS)], \
                            minsize=min_col_size, weight=1)


        #----------------CIRCLES TO SHOW MAIN INFORMATION-------------------
        xPosCirc = min_col_size - min_row_size
        self.circleCanvas = Tk.Canvas( \
                    bg=background_color, bd=0, highlightthickness=0, \
                    width=min_col_size, height=min_row_size)

        #self.circleCanvas.create_oval(xPosCirc, 5, \
        #             xPosCirc + min_row_size, min_row_size - 5, \
        #             width=1.5, outline=foreground_blue, \
        #             fill="")


        #self.circleCanvas.create_oval(xPosCirc + min_row_size, 5, \
        #             xPosCirc + min_row_size*2, min_row_size - 5, \
        #             width=1.5, outline=foreground_blue, \
        #             fill="")

        self.temperature_tk = self.circleCanvas.create_text(xPosCirc + \
                    text_offset - distance_in_between, min_row_size/2,\
                    fill=foreground_blue,\
                    font=font_bold_big,\
                    text=self.DEGREE_S)

        self.humidity_tk = self.circleCanvas.create_text(xPosCirc + \
                    min_row_size + \
                    text_offset + distance_in_between, min_row_size/2,\
                    fill=foreground_blue,\
                    font=font_bold_big,\
                    text="%")

        self.circleCanvas.grid(row=1, column=0, columnspan=2, sticky="nwe")
        #---------------------------------------------------------------






        #-------------------------------CONTROLS--------------------------------
        temp_label = Tk.Label(\
                master=root, \
                text="Temperature control (C˚)", \
                bg=background_color, \
                fg=foreground_color, \
                font=font)

        humidity_label = Tk.Label(\
                master=root, \
                text="Humidity control (%)", \
                bg=background_color, \
                fg=foreground_color, \
                font=font)

        light_label = Tk.Label(\
                master=root, \
                text="Light control (24Hr)", \
                bg=background_color, \
                fg=foreground_color, \
                font=font)

        pump_label = Tk.Label(\
                master=root, \
                text="Pump control", \
                bg=background_color, \
                fg=foreground_color, \
                font=font)

        self.temp_On = TtkScale(root, \
                from_=CONTROLS["TEMPERATURE"]["min_on"], \
                to=CONTROLS["TEMPERATURE"]["max_on"], \
                style="my.Horizontal.TScale",
                bgL=background_color, #for label
                fgL=foreground_color, #for label
                fontL=font,
                #length=int(scale_factor*200), \
                #width=int(scale_factor*25), \
                #label= "ON (+)",\
                #troughcolor=foreground_color, \
                #highlightbackground = foreground_color, \
                #highlightthickness=0, \
                command=(lambda c: self.verify_val(["TEMPERATURE", "ON"])), \
                #bg=background_color, \
                #fg=foreground_color, \
                #font=font, \
                orient='horizontal')

        self.temp_Off = TtkScale(root, \
                from_=CONTROLS["TEMPERATURE"]["min_off"], \
                to=CONTROLS["TEMPERATURE"]["max_off"], \
                style="my.Horizontal.TScale",
                bgL=background_color, #for label
                fgL=foreground_color, #for label
                fontL=font,
                #length=int(scale_factor*200), \
                #width=int(scale_factor*25), \
                #label= "OFF (-)",\
                #troughcolor=foreground_color, \
                #highlightbackground = foreground_color, \
                #highlightthickness=0, \
                command=(lambda c: self.verify_val(["TEMPERATURE", "OFF"])), \
                #bg=background_color, \
                #fg=foreground_color, \
                #font=font, \
                orient='horizontal')


        self.hum_On = Tk.Scale(root, \
                from_=CONTROLS["HUMIDITY"]["min"], \
                to=CONTROLS["HUMIDITY"]["max"], \
                length=int(scale_factor*200), \
                width=int(scale_factor*25), \
                #label= "ON (-)",\
                troughcolor=foreground_color, \
                highlightbackground = foreground_color, \
                highlightthickness=0, \
                command=(lambda c: self.verify_val(["HUMIDITY", "ON"])), \
                bg=background_color, \
                fg=foreground_color, \
                font=font, \
                orient='horizontal')


        self.hum_Off = Tk.Scale(root, \
                from_=CONTROLS["HUMIDITY"]["min"], \
                to=CONTROLS["HUMIDITY"]["max"], \
                length=int(scale_factor*200), \
                width=int(scale_factor*25), \
                #label= "OFF (+)",\
                troughcolor=foreground_color, \
                highlightbackground = foreground_color, \
                highlightthickness=0, \
                command=(lambda c: self.verify_val(["HUMIDITY", "OFF"])), \
                bg=background_color, \
                fg=foreground_color, \
                font=font, \
                orient='horizontal')

        self.light_On = Tk.Scale(root, \
                from_=0, \
                to=24, \
                length=int(scale_factor*200), \
                width=int(scale_factor*25), \
                label= "ON",\
                troughcolor=foreground_color, \
                highlightbackground=foreground_color, \
                highlightthickness=0, \
                command=(lambda c: self.save_all_values("LIGHTON")), \
                bg=background_color, \
                fg=foreground_color, \
                font=font, \
                orient='horizontal')


        self.light_Off = Tk.Scale(root, \
                from_=0, \
                to=24, \
                length=int(scale_factor*200), \
                width=int(scale_factor*25), \
                label= "OFF",\
                troughcolor=foreground_color, \
                highlightbackground = foreground_color, \
                highlightthickness=0, \
                command=(lambda c: self.save_all_values("LIGHTOFF")), \
                bg=background_color, \
                fg=foreground_color, \
                font=font, \
                orient='horizontal')

        self.pump_On = Tk.Scale(root, \
                from_=1, \
                to=4, \
                length=int(scale_factor*200), \
                width=int(scale_factor*25), \
                label= "Times a day",\
                troughcolor=foreground_color, \
                highlightbackground = foreground_color, \
                highlightthickness=0, \
                command=(lambda c: self.save_all_values("PUMPON")), \
                bg=background_color, \
                fg=foreground_color, \
                font=font, \
                orient='horizontal')


        self.pump_Off = Tk.Scale(root, \
                from_=0, \
                to=90, \
                resolution=10, \
                length=int(scale_factor*200), \
                width=int(scale_factor*25), \
                label= "for (seconds)",\
                troughcolor=foreground_color, \
                highlightbackground = foreground_color, \
                highlightthickness=0, \
                command=(lambda c: self.save_all_values("PUMPOFF")), \
                bg=background_color, \
                fg=foreground_color, \
                font=font, \
                orient='horizontal')


        self.temp_On.set(CONTROLS["TEMPERATURE"]["default_on"])
        self.temp_Off.set(CONTROLS["TEMPERATURE"]["default_off"])

        self.hum_On.set(CONTROLS["HUMIDITY"]["default_on"])
        self.hum_Off.set(CONTROLS["HUMIDITY"]["default_off"])

        self.light_On.set(CONTROLS["LIGHT"]["default_on"])
        self.light_Off.set(CONTROLS["LIGHT"]["default_off"])

        self.pump_On.set(CONTROLS["PUMP"]["default_on"])
        self.pump_Off.set(CONTROLS["PUMP"]["default_off"])



        temp_label.grid(row=2,column=0, columnspan=2, sticky="swe")
        self.temp_On.grid(row=3,column=1,sticky="nwe")
        self.temp_Off.grid(row=3,column=0,sticky="nwe")

        humidity_label.grid(row=4,column=0, columnspan=2, sticky="swe")
        self.hum_On.grid(row=5,column=0,sticky="nwe")
        self.hum_Off.grid(row=5,column=1,sticky="nwe")

        light_label.grid(row=6,column=0, columnspan=2, sticky="swe")
        self.light_On.grid(row=7,column=0,sticky="nwe")
        self.light_Off.grid(row=7,column=1,sticky="nwe")

        pump_label.grid(row=8,column=0, columnspan=2, sticky="swe")
        self.pump_On.grid(row=9,column=0,sticky="nwe")
        self.pump_Off.grid(row=9,column=1,sticky="nwe")
        #---------------------------------------------------------------------

        self.all_values = {}
        self.pump_Hour = self.pump_On.get()
        self.pump = 0
        self.light = 0
        self.save_all_values("INI")


    def verify_val(self, val):

        save_values = True

        if val[0] == "TEMPERATURE":
            v_on  = self.temp_On.get()
            v_off = self.temp_Off.get()

            if val[1] == "OFF":

                if v_on <= v_off:
                    save_values = False

                    #mb.showerror("Range error", \
                    #"Increase temperature range first")
                    self.temp_Off.set(self.all_values["".join(val)])


            if val[1] == "ON":

                if v_on <= v_off:
                    save_values = False
                    self.temp_On.set(self.all_values["".join(val)])
                    #mb.showerror("Range error", \
                    #"Decrease temperature range first")


        if val[0] == "HUMIDITY":

            v_on  = self.hum_On.get()
            v_off = self.hum_Off.get()

            if val[1] == "OFF":

                if v_on >= v_off:
                    save_values = False
                    self.hum_Off.set(self.all_values["".join(val)])
                    #mb.showerror("Range error", \
                    #"Decrease humidity range first")


            if val[1] == "ON":

                if v_on >= v_off:
                    self.hum_On.set(self.all_values["".join(val)])
                    #mb.showerror("Range error", \
                    #"Increase humidity range first")
                    save_values = False


        if save_values:
            self.save_all_values("".join(val))


    def save_all_values(self, key):

        self.all_values = \
            {
            "TEMPERATUREON"  : self.temp_On.get(), \
            "TEMPERATUREOFF" : self.temp_Off.get(), \
            "HUMIDITYON"      : self.hum_On.get(), \
            "HUMIDITYOFF"     : self.hum_Off.get(), \
            "LIGHTON"         : self.light_On.get(), \
            "LIGHTOFF"        : self.light_Off.get(), \
            "PUMPON"          : self.pump_On.get(), \
            "PUMPOFF"        : self.pump_Off.get()
            }

        self.adjust_values_of_control()

    def adjust_values_of_control(self):

        now = datetime.datetime.now().hour

        if now >= self.all_values["LIGHTON"] and \
           now <  self.all_values["LIGHTOFF"]:

           #print("ON light")
           self.light = 1

        else:
            #print("OFF light")
            self.light = 0


        if now >= self.all_values["LIGHTON"] and \
           now <  self.all_values["LIGHTOFF"]:

           self.light = 1

        t0 = self.all_values["LIGHTON"]
        mult = int(24/self.all_values["PUMPON"])
        hrs = [x for x in range(0, 24, mult)]
        #2 - 8 + 6 = 0
        #8 - 8 + 6 = 6
        #14 - 8 + 6 = 12
        #20 - 8 + 6 = 18

        if (now - t0 + mult) in hrs and self.pump_Hour != now:
            self.pump_Hour = now
            self.pump = 1
            runP = Timer(float(self.all_values["PUMPOFF"]), self.run_pump_fun)
            runP.start()


    def run_pump_fun(self):

        self.pump = 0
        #print("pumpf OFFFF F")




    def update_temp_hum(self, temp, hum):

        self.circleCanvas.itemconfigure(self.humidity_tk, text= str(hum) +"%")
        self.circleCanvas.itemconfigure(self.temperature_tk, \
                                text= str(temp) + self.DEGREE_S)

    def get_all_values(self):

        result =  \
                "T" + str(self.temp_On.get()) + \
                "t" + str(self.temp_Off.get()) + \
                "H" + str(self.hum_On.get()) + \
                "h" + str(self.hum_Off.get()) + \
                "L" + str(self.light) + \
                "l" + str(int(not self.light)) + \
                "P" + str(int(self.pump)) + \
                "p" + str(int(not self.pump)) + "\n"

        return result
